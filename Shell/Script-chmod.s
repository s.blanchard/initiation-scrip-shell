#!/bin/bash
 
  for i in "$@"
  do
          if [ -f $i ]
          then
                  if [[ -r $i ]]
                  then
                         chmod u+r $@
                         echo "le fichier '$i' a obtenu les droits de lecture pour le propriétaire"
                 fi

         elif [ -d $i ]
         then
                 if [[ -x $i ]]
                 then
                         chmod a+x $@
                         echo "le répertoire '$i' a obtenu les droits d'écriture pour tout le monde"
                 fi
         else
                 echo "$@ n'est ni un fichier ordinaire ni un répertoire"
         fi
done