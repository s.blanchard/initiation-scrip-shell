#!/bin/bash

if [ $1 ]
then
        if [ -f $1 ]
        then
            echo "Type du fichier '$1' : "
            file -b $1
            echo "Droits d'accès : "
            stat -c '%A (%a)' $1
else
        echo "Aucun fichier ordinaire correct n'a été fourni en paramètre"
fi
