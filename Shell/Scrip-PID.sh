#!/bin/bash
if [ $1 ]
then
        echo "PID du programme $1 = "
         ps -es | grep $1 | awk '{ print $1  }'
else
        echo "Aucun nom de programme n’a été fourni"
fi
